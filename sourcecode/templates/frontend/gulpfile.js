var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var autoprefix = require('gulp-autoprefixer');
var del = require('del');
var sourcemaps = require('gulp-sourcemaps');
var flatten = require('gulp-flatten');
var gutil = require('gulp-util');
var connect = require('gulp-connect');

var FILES = {
    'src': 'src',
    'js': 'src/js',
    'scss': 'src/scss',
    'fonts': 'src/fonts',
    'images': 'src/images',
    'dest': 'public',
    'destCss': 'public/css',
    'destFonts': 'public/fonts',
    'destJs': 'public/js',
    'destImg': 'public/images',
};

var imgFiles = FILES.images + '/**/*.{png,jpg,jpeg,svg}';

var config = {
    bootstrapDir: 'node_modules/bootstrap-sass',
    awesomeDir: 'node_modules/font-awesome',
    opensanDir: 'node_modules/npm-font-open-sans'
};

gulp.task('connect', function () {
    return connect.server({
        root: '',
        livereload: true,
        port: 8080
    });
});

gulp.task('clean-css', function() {
    return del([FILES.destCss]);
});

gulp.task('clean-js', function() {
    return del([FILES.destJs]);
});

gulp.task('clean-fonts', function() {
    return del([FILES.destFonts]);
});

gulp.task('clean-img', function() {
    return del([FILES.destImg]);
});

gulp.task('copy-bootstrap-fonts', ['clean-fonts'], function() {
  return gulp.src(config.bootstrapDir + '/assets/fonts/bootstrap/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest(FILES.fonts));
});

gulp.task('copy-awesome-fonts', ['clean-fonts'], function() {
  return gulp.src(config.awesomeDir + '/fonts/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest(FILES.fonts));
});

gulp.task('copy-font-opensan', ['clean-fonts'], function() {
  return gulp.src(config.opensanDir + '/fonts/*/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest(FILES.destFonts));
});

gulp.task('copy-fonts', ['clean-fonts'], function() {
    gulp.src(FILES.fonts + '/*/*.{ttf,woff,woff2,eot,svg}')
        .pipe(gulp.dest(FILES.destFonts));
});

gulp.task('copyfonts', ['clean-fonts'], function() {
    gulp.src(FILES.fonts + '/**/*.{ttf,woff,woff2,eot,svg}')
        .pipe(gulp.dest(FILES.destFonts));
});

gulp.task('copyimg', ['clean-img'], function() {
    gulp.src(imgFiles)
        .pipe(gulp.dest(FILES.destImg));
});

gulp.task('scss', ['clean-css'], function() {
    var scssSources = FILES.scss + '/*.scss';
    gulp.src(scssSources)
        // .pipe(sourcemaps.init()) // use for development
        .pipe(sass({outputStyle: 'compressed'}).on('error', gutil.log))
        .pipe(autoprefix({
            'browsers': ['last 2 version', 'ie 9'],
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(FILES.destCss));
});

gulp.task('css', ['clean-css'], function() {
    var scssSources = FILES.scss + '/*.css';
    gulp.src(scssSources)
        // .pipe(sourcemaps.init()) // use for development
        .pipe(sass({outputStyle: 'compressed'}).on('error', gutil.log))
        .pipe(autoprefix({
            'browsers': ['last 2 version', 'ie 9'],
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(FILES.destCss));
});

gulp.task('js', ['clean-js'], function() {
    gutil.log(gutil.colors.magenta('Start Javascript task'));
    var jsSources = FILES.js + '/**/*.js';
    gulp.src(jsSources)
        .pipe(watch(jsSources))
        .pipe(flatten())
        .pipe(gulp.dest(FILES.destJs))
        .on('end', function() { gutil.log('Done Javascript task!'); });
});

gulp.task('default', ['connect', 'scss', 'css', 'copyfonts', 'copy-fonts', 'copy-bootstrap-fonts', 'copy-awesome-fonts', 'copy-font-opensan', 'copyimg', 'js'], function() {
    var scssWatchSources = FILES.scss + '/**/*.scss';
    gulp.watch(scssWatchSources, ['scss']);
    gulp.watch(imgFiles, ['copyimg']);
});