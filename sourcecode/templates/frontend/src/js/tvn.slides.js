var tvnSlideshow = (function() {

	var init = function(settings) {
		var slideModal = $(settings.slideModal),
			prevButton = $(settings.prevButton),
			nextButton = $(settings.nextButton),
			currentHiddenImage = $(settings.currentHiddenImage),
			previousHiddenImage = $(settings.previousHiddenImage),
			nextHiddenImage = $(settings.nextHiddenImage),
			closeButton = $(settings.closeButton),
			imageSlide = $(settings.imageSlide),
			slideNode = $(settings.slideNode);

		var dialogModal = slideModal.find('.modal-dialog');

		var maxWidth = $(window).width() - 100,
			maxHeight = $(window).height() - 100;

		slideNode.on('click', function(e) {

			slideModal.modal('show');
			// get image that clicked
			var currentImage =  $(e.target);
			var imageSrc = currentImage.attr('src'),
				imageNatural = currentImage.attr('data-image');

			var curImageId = currentImage.attr('id'),
				prevImageId = currentImage.attr('data-prev'),
				nextImageId = currentImage.attr('data-next');

			currentHiddenImage.val(curImageId);
			previousHiddenImage.val(prevImageId);
			nextHiddenImage.val(nextImageId);

			var image = new Image();
			image.src = imageNatural;

			if(prevImageId != null && !prevImageId.length) {
				prevButton.hide();
			} else {
				prevButton.show();
			}
			if(nextImageId != null && !nextImageId.length) {
				nextButton.hide();
			} else {
				nextButton.show();
			}

			image.onload = function () {
				var minWidth = Math.min(maxWidth, image.naturalWidth);
				var minHeight = (image.naturalHeight * minWidth)/image.naturalWidth;

				dialogModal.css({
					width: minWidth + 30,
					height: minHeight
				});

				var imageEl = slideModal.find('.tvn-image-show');
				imageEl.css({
					width: minWidth,
					height: minHeight
				});

				imageSlide.attr('src', imageNatural);
			};
		});

        prevButton.on('click', function(e) {
			var curImage = $('#' + previousHiddenImage.val());

			var imageSrc = curImage.attr('src'),
				imageNatural = curImage.attr('data-image');

			var curImageId = curImage.attr('id'),
				prevImageId = curImage.attr('data-prev'),
				nextImageId = curImage.attr('data-next');

			// update value for hidden inputs
			currentHiddenImage.val(curImageId);
			previousHiddenImage.val(prevImageId);
			nextHiddenImage.val(nextImageId);

			var image = new Image();
			image.src = imageNatural;

			if (prevImageId != null && !prevImageId.length) {
				prevButton.hide();
			} else {
				prevButton.show();
			}
			if (nextImageId != null && !nextImageId.length) {
				nextButton.hide();
			} else {
				nextButton.show();
			}

			image.onload = function () {
			var minWidth = Math.min(maxWidth, image.naturalWidth);
			var minHeight = (image.naturalHeight * minWidth) / image.naturalWidth;

			dialogModal.css({
				width: minWidth + 30,
				height: minHeight
			});

			var imageEl = slideModal.find('.tvn-image-show');
			imageEl.css({
				width: minWidth,
				height: minHeight
			});

			imageSlide.attr('src', imageNatural);
			};
		});

		nextButton.on('click', function(e) {
			var curImg = $('#' + nextHiddenImage.val());

			var imageSrc = curImg.attr('src'),
			imageNatural = curImg.attr('data-image');

			var curImageId = curImg.attr('id'),
				prevImageId = curImg.attr('data-prev'),
				nextImageId = curImg.attr('data-next');

			// update value for hidden inputs
			currentHiddenImage.val(curImageId);
			previousHiddenImage.val(prevImageId);
			nextHiddenImage.val(nextImageId);

			if (prevImageId != null && !prevImageId.length) {
				prevButton.hide();
			} else {
				prevButton.show();
			}
			if (nextImageId != null && !nextImageId.length) {
				nextButton.hide();
			} else {
				nextButton.show();
			}

			var image = new Image();
			image.src = imageNatural;

			var minWidth = Math.min.apply(Math, [maxWidth, image.naturalWidth]);
			var minHeight = (image.naturalHeight * minWidth) / image.naturalWidth;

			image.onload = function () {
				var minWidth = Math.min(maxWidth, image.naturalWidth);
				var minHeight = (image.naturalHeight * minWidth) / image.naturalWidth;

				dialogModal.css({
					width: minWidth + 30,
					height: minHeight
				});

				var imageEl = slideModal.find('.tvn-image-show');
				imageEl.css({
					width: minWidth,
					height: minHeight
				});

				imageSlide.attr('src', imageNatural);
			};
		});

		closeButton.click(function() {
			currentHiddenImage.val('');
			previousHiddenImage.val('');
			nextHiddenImage.val('');
		});
	};

	return {
		init: init
	};

}).call(this);