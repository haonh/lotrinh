import { TriprouteAppPage } from './app.po';

describe('triproute-app App', () => {
  let page: TriprouteAppPage;

  beforeEach(() => {
    page = new TriprouteAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
