export interface Logined {
    username: string;
    password: string;
    id: number;
    rememberMe?: boolean;
}

export interface User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    address: string;
    mobile: string;
    email: string;
    imageUrl: string;
    activated?: boolean;
    activationKey: string;
    langKey?: string;
    resetKey?: string;
    createdBy?: string;
    createdDate: Date;
    resetDate?: Date;
    lastModifiedBy?: string;
    lastModifiedDate?: Date;
}