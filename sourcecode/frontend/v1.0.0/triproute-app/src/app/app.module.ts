import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DatePipe } from '@angular/common';

import { AppComponent } from './app.component';
import { AdminModule } from './admin/admin.module';
import './rxjs-functions';

// import {
//   ConfirmPasswordDirective
// } from './shared/custom_validators'

import {
  MainComponent,
  HeaderComponent,
  SidebarComponent,
  FooterComponent,
  ErrorComponent
} from './layouts'


@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    ErrorComponent,
    //ConfirmPasswordDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AdminModule
  ],
  providers: [
    DatePipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [MainComponent]
})
export class AppModule { }
