import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    SharedLibsModule,
    SharedCommonModule
} from './';

@NgModule({
    imports: [
        SharedLibsModule,
        SharedCommonModule
    ],
    declarations: [
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [],
    exports: [
        SharedCommonModule,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class SharedModule {}
