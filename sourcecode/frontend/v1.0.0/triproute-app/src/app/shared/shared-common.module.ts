import { NgModule, Sanitizer } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { AuthenticationService } from './services/authentication.service';


import {
    SharedLibsModule,
} from './';


@NgModule({
    imports: [
        SharedLibsModule
    ],
    declarations: [
    ],
    providers: [
        Title,
        AuthenticationService
    ],
    exports: [
        SharedLibsModule,
    ]
})
export class SharedCommonModule {}
