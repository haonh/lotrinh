import { Directive, forwardRef, Input, OnChanges, Provider } from '@angular/core';
import { SimpleChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { validate } from 'codelyzer/walkerFactory/walkerFn';

export const CONFIRM_PASSWORD_VALIDATOR: Provider = {
  provide: NG_VALIDATORS,
  useExisting: forwardRef(() => ConfirmPasswordDirective),
  multi: true
}

@Directive({
  selector: '[cfrmPass]',
  providers: [CONFIRM_PASSWORD_VALIDATOR],
  host: { '[attr.cfrmPass]' : 'cfrmPass ? "" : null' }
})

export class ConfirmPasswordDirective implements Validator, OnChanges {
  private _cfrmPassword: boolean;
  private _control: AbstractControl;
  @Input()
  get cfrmPass () {
    return this._cfrmPassword;
  }
  set cfrmPass (value: boolean) {
    this._cfrmPassword = value != null && value !== false
    if (this._onChange) this._onChange();
  }

  constructor () {}

  private _onChange () {
    if (this._control)
      this._control.updateValueAndValidity();
  }

  ngOnChanges (changes: SimpleChanges) {
    if ('cfrmPass' in changes) {
      if (this._onChange) this._onChange();
    }
  }

  validate (c: AbstractControl) {
    this._control = c;
    const val = c.value;
    if (this.cfrmPass) {
      if (val.password == val.confirmPassword) {
        return null;
      } else {
        return {
          confirmPassword: {
            notMatch: true
          }
        };
      }
    }
    return null;
  }
}
