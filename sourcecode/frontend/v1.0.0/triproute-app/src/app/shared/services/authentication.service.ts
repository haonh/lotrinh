import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { tokenNotExpired } from 'angular2-jwt';

import { Logined } from './../../interfaces/user';

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor (private http: Http) {
        let userStorage = '';
        try {
            userStorage = localStorage.getItem('currentUser');
        } catch (err) {}
        const currentUser = JSON.parse(userStorage);
        this.token = currentUser && currentUser.token;
    }

    // login
    login (logined: Logined): Observable<boolean> {
        const headers = new Headers({
            'Content-type': 'application/json'
        });

        const options = new RequestOptions({headers});
        return this.http.post('link api', JSON.stringify(logined), options)
            .map((response: Response) => response.json())
            .map((response) => {
                const token = response.id_token;
                if (token) {
                    this.token = token;
                    try {
                        localStorage.setItem('currentUser', JSON.stringify({ username: logined.username, token: token }));
                    } catch (err) {}
                    return true;
                } else {
                    return false;
                }
            });
    }

    // logout
    logout(): void {
        // clear token remove logined from local storage to log user out
        this.token = null;
        try {
            localStorage.removeItem('currentUser');
        } catch (err) {

        }
    }

    isLoggedIn () {
        return this.token && tokenNotExpired(undefined, this.token);
    }
}
