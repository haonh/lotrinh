import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { SharedModule } from '../shared';

import {
  adminState,
    AdminLoginComponent,
    DashboardComponent,
    PostListManagementComponent,
    PostAddManagementComponent,
    UserManagementListComponent,
    UserManagementAddComponent,
    DestinationManagementComponent,
    DestinationModalComponent,
    DestinationManagementModalComponent,
    DestinationModalService,
    CountryManagementComponent,
    CountryModalComponent,
    CountryManagementModalComponent,
    CountryModalService,
    CategoryManagementComponent,
    CategoryModalComponent,
    CategoryManagementModalComponent,
    CategoryServiceManagementComponent,
    CategoryServiceManagementModalComponent,
    CategoryServiceModalComponent,
    CategoryModalService,
    ServiceListManagementComponent,
    ServiceAddManagementComponent,
    RouteListManagementComponent
} from './';

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        RouterModule.forRoot(adminState, { useHash: true })
    ],
    declarations: [
        AdminLoginComponent,
        DashboardComponent,
        PostListManagementComponent,
        PostAddManagementComponent,
        UserManagementListComponent,
        UserManagementAddComponent,
        DestinationManagementComponent,
        DestinationModalComponent,
        DestinationManagementModalComponent,
        CountryManagementComponent,
        CountryManagementModalComponent,
        CountryModalComponent,
        CategoryManagementComponent,
        CategoryManagementModalComponent,
        CategoryServiceManagementComponent,
        CategoryServiceManagementModalComponent,
        CategoryServiceModalComponent,
        CategoryModalComponent,
        ServiceListManagementComponent,
        ServiceAddManagementComponent,
        RouteListManagementComponent
    ],
    entryComponents: [
        DestinationManagementModalComponent,
        CountryManagementModalComponent,
        CategoryManagementModalComponent,
        CategoryServiceManagementModalComponent
    ],
    providers: [
        DestinationModalService,
        CountryModalService,
        CategoryModalService
    ],
    exports: [
      RouterModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AdminModule {}
