import { Routes } from '@angular/router';

import { UserManagementListComponent } from './user-list.component';
import { UserManagementAddComponent } from './user-add.component';

export const userManagementRoutes = [
    {
      path: 'user-management',
      component: UserManagementListComponent,
      data: {
          pageTitle: 'Danh sách tài khoản hệ thống'
      }
    },
    {
      path: 'user-management-deleted/:state',
      component: UserManagementListComponent,
      data: {
          pageTitle: 'Danh sách tài khoản hệ thống đã xóa'
      }
    },
    {
      path: 'user-management-add',
      component: UserManagementAddComponent,
      data: {
          pageTitle: 'Thêm mới tài khoản'
      }
    },
    {
      path: 'user-management-edit/:id',
      component: UserManagementAddComponent,
      data: {
          pageTitle: 'Cập nhật tài khoản'
      }
    }
];
