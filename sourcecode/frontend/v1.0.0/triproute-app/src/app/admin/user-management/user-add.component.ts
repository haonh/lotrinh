import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { User } from '../../interfaces/user';
import { validate } from 'codelyzer/walkerFactory/walkerFn';

export function comparePassword(c: AbstractControl) {
  const v = c.value;
    return (v.password == v.confirmPassword) ? null : {
      passwordnotmatch: true
    };
}

@Component({
    selector: 'app-user-add',
    templateUrl: 'user-add.component.html'
})

export class UserManagementAddComponent implements OnInit, OnDestroy {

    userForm: FormGroup;
    user: any = {};
    userId: number;

    constructor (
        private fb: FormBuilder,
        private router: Router,
        private routeParams: ActivatedRoute
    ) {}

    ngOnInit () {
        this.routeParams.params.subscribe(params => {
          this.userId = +params['id'];
        })
        if (this.userId > 0) {

        }

        this.userForm = this.fb.group({
          username: ['', [Validators.required, Validators.maxLength(50)]],
          pw: this.fb.group({
            password: ['', Validators.required, Validators.minLength(8)],
            confirmPassword: ['', Validators.required]
          }, {
            validator: comparePassword
          }),
          firstname: ['', [Validators.required, Validators.maxLength(50)]],
          lastname: ['', [Validators.required, Validators.maxLength(50)]],
          address: [''],
          mobile: [''],
          email: ['', [Validators.required, Validators.email]],
          imageUrl: [''],
          activated: ['']
        });


    }

    ngOnDestroy () {}

    createUser () {}
}
