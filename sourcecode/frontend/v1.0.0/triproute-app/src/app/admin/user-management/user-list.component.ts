import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-user-list',
    templateUrl: 'user-list.component.html',
    styleUrls: [
        'user-management.style.scss'
    ]
})

export class UserManagementListComponent implements OnInit, OnDestroy {
    stateParam: String = '';
    constructor (
        private router: Router,
        private _routeParams: ActivatedRoute
    ) {}

    ngOnInit () {
        this._routeParams.params.subscribe(params => {
            this.stateParam = params['state'];
        });
    }
    ngOnDestroy () {}

    goToCreateUser () {
        this.router.navigate(['admin/user-management-add']);
    }
}