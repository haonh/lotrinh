import { Routes } from '@angular/router';

import { ServiceListManagementComponent } from './service-list-management.component';
import { ServiceAddManagementComponent } from './service-add-management.component';

export const serviceManagementRoutes = [
  {
    path: 'service-management',
    component: ServiceListManagementComponent,
    data: {
        pageTitle: 'Quản lý thông tin dịch vụ'
    }
  },
  {
    path: 'service-management-add',
    component: ServiceAddManagementComponent,
    data: {
        pageTitle: 'Quản lý thông tin dịch vụ | Thêm mới dịch vụ'
    }
  }
];
