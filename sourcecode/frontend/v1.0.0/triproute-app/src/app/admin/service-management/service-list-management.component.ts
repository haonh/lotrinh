import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-service-management',
    templateUrl: 'service-list-management.component.html'
})

export class ServiceListManagementComponent implements OnInit, OnDestroy {
    id: number;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit () {}

    ngOnDestroy () {}

    goToCreate () {
        this.router.navigate(['admin/service-management-add']);
    }
}
