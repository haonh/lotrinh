import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validator, Validators } from '@angular/forms';

import 'tinymce/themes/modern/theme';
import 'assets/tinymce/plugins/autolink/plugin.js';
import 'assets/tinymce/plugins/image/plugin.js';
import 'assets/tinymce/plugins/charmap/plugin.js';
import 'assets/tinymce/plugins/print/plugin.js';
import 'assets/tinymce/plugins/link/plugin.js';
import 'assets/tinymce/plugins/paste/plugin.js';
import 'assets/tinymce/plugins/table/plugin.js';
import 'assets/tinymce/plugins/advlist/plugin.js';
import 'assets/tinymce/plugins/autoresize/plugin.js';
import 'assets/tinymce/plugins/lists/plugin.js';
import 'assets/tinymce/plugins/code/plugin.js';
import 'assets/tinymce/plugins/preview/plugin.js';
import 'assets/tinymce/plugins/anchor/plugin.js';
import 'assets/tinymce/plugins/searchreplace/plugin.js';
import 'assets/tinymce/plugins/visualblocks/plugin.js';
import 'assets/tinymce/plugins/fullscreen/plugin.js';
import 'assets/tinymce/plugins/insertdatetime/plugin.js';
import 'assets/tinymce/plugins/media/plugin.js';
import 'assets/tinymce/plugins/contextmenu/plugin.js';

@Component({
    selector: 'app-post-add',
    templateUrl: 'post-add-management.component.html'
})

export class PostAddManagementComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() elementId: string;
    @Output() onEditorKeyup = new EventEmitter<any>();
    public editor:any;
    baseURL: string = '/';
    postForm: FormGroup;

    categories: any = [];

    constructor (
        private fb: FormBuilder,
        private router: Router
    ) {}

    ngOnInit () {
      this.postForm = this.fb.group({
        infoForm: this.fb.group({
          title: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(300)]],
          address: ['', [Validators.required]],
          description: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(350)]],
          content: ['', [Validators.required, Validators.minLength(20)]],
          imageUrl: ['', []],
          categoryId: ['', []],
          countryId: ['', []],
          status: false
          }),
        sightForm: this.fb.group({
          priceInfo: ['', [Validators.maxLength(250)]],
          hourInfo: ['', [Validators.maxLength(250)]],
          locationInfo: ['', [Validators.maxLength(350)]],
          mapUrl: ['', [Validators.maxLength(250)]]
        })
      });

      this.categories = this.getCategories();
    }

    ngAfterViewInit () {
        tinymce.init({
            selector: 'textarea#content',
            base_url: '/assets/',
            plugins: ['advlist autolink lists link image charmap print preview anchor',
                            'searchreplace visualblocks code fullscreen',
                            'insertdatetime media table contextmenu paste code'],
            // toolbar: ['undo redo | bold italic | alignleft aligncenter alignright | code'],
            skin_url: this.baseURL + 'assets/tinymce/skins/lightgray',
            setup: editor => {
                this.editor = editor;
                editor.on('keyup', () => {
                const content = editor.getContent();
                this.onEditorKeyup.emit(content);
                });
            },
        });
    }

    ngOnDestroy () {
        tinymce.remove(this.editor);
    }

    createPost () {}

    onChange(event: any, uploaded: any) {
        let files = [].slice.call(event.target.files);
        uploaded.insertAdjacentHTML('beforeend', files.map(f => f.name).join(', '));
    }

    private getCategories () {
      return [
        {
          id: 0,
          name: '--- Danh mục ---'
        },
        {
          id: 1,
          name: 'Địa danh'
        },
        {
          id: 2,
          name: 'Danh lam thắng cảnh'
        }
      ]
    }
}
