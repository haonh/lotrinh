import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-post-management',
    templateUrl: 'post-list-management.component.html'
})

export class PostListManagementComponent implements OnInit, OnDestroy {
    id: number;
    stateParam: String = '';

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit () {
        this.activatedRoute.params.subscribe(params => {
            this.stateParam = params['state'];
        });
    }

    ngOnDestroy () {}

    goToCreate () {
        this.router.navigate(['admin/post-management-add']);
    }
}