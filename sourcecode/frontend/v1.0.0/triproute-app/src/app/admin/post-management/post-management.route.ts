import { Routes } from '@angular/router';

import { PostListManagementComponent } from './post-list-management.component';
import { PostAddManagementComponent } from './post-add-management.component';

export const postManagementRoutes = [
  {
    path: 'post-management',
    component: PostListManagementComponent,
    data: {
        pageTitle: 'Quản lý thông tin bài viết'
    }
  },
  {
    path: 'post-management-add',
    component: PostAddManagementComponent,
    data: {
        pageTitle: 'Quản lý thông tin bài viết | Thêm mới bài viết'
    }
  }
];
