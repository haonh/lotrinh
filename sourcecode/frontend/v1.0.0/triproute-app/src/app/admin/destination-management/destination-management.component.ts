import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { DestinationModalService } from './destination-modal.service';
import { DestinationManagementModalComponent } from './destination-management-modal.component'

@Component({
    selector: 'app-destination-management',
    templateUrl: 'destination-management.component.html'
})

export class DestinationManagementComponent implements OnInit, OnDestroy {
    id: number;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private destinationModalService: DestinationModalService
    ) {}

    ngOnInit () {}

    ngOnDestroy () {}

    createNewDestination () {
        this.destinationModalService.open(DestinationManagementModalComponent);
    }

    editDestination (id) {
        this.destinationModalService.open(DestinationManagementModalComponent, id);
    }
}