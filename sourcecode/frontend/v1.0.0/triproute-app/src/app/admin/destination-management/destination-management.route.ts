import { Routes } from '@angular/router';

import { DestinationManagementComponent } from './destination-management.component';
import { DestinationModalComponent } from './destination-management-modal.component';

export const destinationManagementRoutes = [
    {
        path: 'destination-management',
        component: DestinationManagementComponent,
        data: {
            pageTitle: 'Quản lý thông tin khu vực - châu lục trên thế giới'
        }
    }
];

export const destinationModalRoutes: Routes = [
  {
    path: 'destination-management-add',
    component: DestinationModalComponent,
    outlet: 'popup'
  },
  {
    path: 'destination-management-edit/:id',
    component: DestinationModalComponent,
    outlet: 'popup'
  }
];