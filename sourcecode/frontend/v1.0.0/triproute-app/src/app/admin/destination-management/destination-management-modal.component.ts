import { Component, OnInit, AfterViewInit, Renderer, ElementRef, OnDestroy } from '@angular/core';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { DestinationModalService } from './destination-modal.service';

@Component ({
    selector: 'app-destination-mng-modal',
    templateUrl: './destination-management-modal.component.html'
})

export class DestinationManagementModalComponent implements OnInit {
    destinationForm: FormGroup;

    constructor (
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
    ) {}

    ngOnInit () {
        this.destinationForm = this.fb.group({
            destinationNameVN: ['', [Validators.required]],
            destinationNameEN: ['', [Validators.required]],
            latitude: [''],
            longitude: [''],
            status: false
        });
    }

    createDestination () {}

    cancel () {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'app-destination-modal',
    template: ''
})

export class DestinationModalComponent implements OnInit, OnDestroy {
    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private destinationModalService: DestinationModalService
    ) {}

    ngOnInit () {
        this.routeSub = this.route.params.subscribe(params => {
            if (params['id']) {
                this.modalRef = this.destinationModalService.open(DestinationManagementModalComponent, params['id']);
            } else {
                this.modalRef = this.destinationModalService.open(DestinationManagementModalComponent);
            }
        });
    }

    ngOnDestroy () {
        this.routeSub.unsubscribe();
    }
}
