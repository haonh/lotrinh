import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-route-management',
    templateUrl: 'route-list-management.component.html'
})

export class RouteListManagementComponent implements OnInit, OnDestroy {
    id: number;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit () {}

    ngOnDestroy () {}

    goToCreate () {
        this.router.navigate(['admin/route-management-add']);
    }
}
