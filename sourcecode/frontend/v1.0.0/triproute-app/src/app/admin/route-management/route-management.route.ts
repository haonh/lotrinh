import { Routes } from '@angular/router';

import { RouteListManagementComponent } from './route-list-management.component';

export const routeManagementRoutes = [
  {
    path: 'route-management',
    component: RouteListManagementComponent,
    data: {
        pageTitle: 'Quản lý thông tin lộ trình'
    }
  }
];
