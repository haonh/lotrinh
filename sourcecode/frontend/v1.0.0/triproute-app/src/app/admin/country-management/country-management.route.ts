import { Routes } from '@angular/router';

import { CountryManagementComponent } from './country-management.component';
import { CountryModalComponent } from './country-management-modal.component';

export const countryManagementRoutes = [
    {
        path: 'country-management',
        component: CountryManagementComponent,
        data: {
            pageTitle: 'Quản lý thông tin khu vực - quốc gia trên thế giới'
        }
    }
];

export const countryModalRoutes: Routes = [
  {
    path: 'country-management-add',
    component: CountryManagementComponent,
    outlet: 'popup'
  },
  {
    path: 'country-management-edit/:id',
    component: CountryManagementComponent,
    outlet: 'popup'
  }
];