import { Component, OnInit, AfterViewInit, Renderer, ElementRef, OnDestroy } from '@angular/core';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { CountryModalService } from './country-modal.service';

@Component ({
    selector: 'app-country-mng-modal',
    templateUrl: './country-management-modal.component.html'
})

export class CountryManagementModalComponent implements OnInit {
    countryForm: FormGroup;

    constructor (
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
    ) {}

    ngOnInit () {
        this.countryForm = this.fb.group({
            countryNameVN: ['', [Validators.required]],
            countryNameEN: ['', [Validators.required]],
            destinationId: ['', [Validators.required]],
            latitude: [''],
            longitude: [''],
            status: false
        });
    }

    createCountry () {}

    cancel () {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'app-country-modal',
    template: ''
})

export class CountryModalComponent implements OnInit, OnDestroy {
    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private countryModalService: CountryModalService
    ) {}

    ngOnInit () {
        this.routeSub = this.route.params.subscribe(params => {
            if (params['id']) {
                this.modalRef = this.countryModalService.open(CountryManagementModalComponent, params['id']);
            } else {
                this.modalRef = this.countryModalService.open(CountryManagementModalComponent);
            }
        });
    }

    ngOnDestroy () {
        this.routeSub.unsubscribe();
    }
}
