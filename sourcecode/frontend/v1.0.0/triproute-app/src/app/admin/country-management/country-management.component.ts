import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { CountryModalService } from './country-modal.service';
import { CountryManagementModalComponent } from './country-management-modal.component'

@Component({
    selector: 'app-country-management',
    templateUrl: 'country-management.component.html'
})

export class CountryManagementComponent implements OnInit, OnDestroy {
    id: number;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private countryModalService: CountryModalService
    ) {}

    ngOnInit () {}

    ngOnDestroy () {}

    createNewCountry () {
        this.countryModalService.open(CountryManagementModalComponent);
    }
}