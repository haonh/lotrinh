import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-triproute-adm-dashboard',
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit {
    constructor (
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit () {}
}