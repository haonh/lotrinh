import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators  } from '@angular/forms';

import { AuthenticationService } from '../../shared/services/authentication.service';


@Component({
    selector: 'app-adm-login',
    templateUrl: 'login.component.html',
    styleUrls: [
        'login.scss'
    ]
})

export class AdminLoginComponent implements OnInit {

    loginForm: FormGroup;
    recoverForm: FormGroup;

    protected hideRecoverForm: Boolean = true;
    protected hideLoginForm: Boolean = false;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private authService: AuthenticationService
    ) {}

    ngOnInit () {
        this.loginForm = this.fb.group({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
            rememberMe: false,
        });
        this.recoverForm = this.fb.group({
            email: new FormControl('', [Validators.required])
        });

        if (this.authService.isLoggedIn()) {
          this.router.navigate(['admin/dashboard']);
          return;
        }
    }

    goRecoverForm () {
        if (this.hideRecoverForm) {
            this.hideRecoverForm = !this.hideRecoverForm;
            this.hideLoginForm = !this.hideLoginForm;
        }
    }

    backLoginForm () {
        if (this.hideLoginForm) {
            this.hideLoginForm = !this.hideLoginForm;
            this.hideRecoverForm = !this.hideRecoverForm;
        }
    }

    login () {
      this.authService.login(this.loginForm.value).subscribe((res) => {
        if (res) {
          this.router.navigate(['admin/dashboard']);
        } else {
          console.log('Có lỗi xảy ra');
        }
      })

    }

    recover () {
        console.log('vao recover');
    }
}
