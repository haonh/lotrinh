import { Route } from '@angular/router';

import { AdminLoginComponent } from './login.component';

export const adminLoginRoute: Route = {
    path: 'login',
    component: AdminLoginComponent,
    data: {
        pageTitle: 'Administrator Panel | Login'
    }
};
