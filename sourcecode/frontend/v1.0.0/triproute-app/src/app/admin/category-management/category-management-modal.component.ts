import { Component, OnInit, AfterViewInit, Renderer, ElementRef, OnDestroy } from '@angular/core';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { CategoryModalService } from './category-modal.service';

@Component ({
    selector: 'app-country-mng-modal',
    templateUrl: './category-management-modal.component.html'
})

export class CategoryManagementModalComponent implements OnInit {
    categoryForm: FormGroup;

    constructor (
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
    ) {}

    ngOnInit () {
        this.categoryForm = this.fb.group({
            id: [''],
            categoryName: ['', [Validators.required, Validators.max(200)]],
            parentId: '',
            status: false
        });
    }

    createCategory () {}

    cancel () {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'app-category-modal',
    template: ''
})

export class CategoryModalComponent implements OnInit, OnDestroy {
    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private categoryModalService: CategoryModalService
    ) {}

    ngOnInit () {
        this.routeSub = this.route.params.subscribe(params => {
            if (params['id']) {
                this.modalRef = this.categoryModalService.open(CategoryManagementModalComponent, params['id']);
            } else {
                this.modalRef = this.categoryModalService.open(CategoryManagementModalComponent);
            }
        });
    }

    ngOnDestroy () {
        this.routeSub.unsubscribe();
    }
}
