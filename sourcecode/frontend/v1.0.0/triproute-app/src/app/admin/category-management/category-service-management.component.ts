import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { CategoryModalService } from './category-modal.service';
import { CategoryServiceManagementModalComponent } from './category-service-management-modal.component'

@Component({
    selector: 'app-category-management',
    templateUrl: 'category-service-management.component.html'
})

export class CategoryServiceManagementComponent implements OnInit, OnDestroy {
    id: number;

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private categoryModalService: CategoryModalService
    ) {}

    ngOnInit () {}

    ngOnDestroy () {}

    createNewCategory () {
        this.categoryModalService.open(CategoryServiceManagementModalComponent);
    }
}
