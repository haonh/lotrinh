import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { CategoryManagementModalComponent } from './category-management-modal.component';

@Injectable ()
export class CategoryModalService {
    private isOpen: boolean = false;

    constructor (
        private modalService: NgbModal,
        private router: Router
    ) {
        if (this.isOpen) {
            return;
        }
    }

    open (component: Component, id?: number): NgbModalRef {
        if (this.isOpen) {
            return;
        }

        this.isOpen = true;
        return this.categoryModalRef(component);
    }

    categoryModalRef (component: Component): NgbModalRef {
        let modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.result.then(result => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}