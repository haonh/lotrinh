import { Component, OnInit, AfterViewInit, Renderer, ElementRef, OnDestroy } from '@angular/core';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { CategoryModalService } from './category-modal.service';

@Component ({
    selector: 'app-category-service-modal',
    templateUrl: './category-service-management-modal.component.html'
})

export class CategoryServiceManagementModalComponent implements OnInit {
    categoryForm: FormGroup;

    constructor (
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
    ) {}

    ngOnInit () {
        this.categoryForm = this.fb.group({
            id: [''],
            categoryName: ['', [Validators.required, Validators.max(200)]],
            parentId: '',
            status: false
        });
    }

    createCategory () {}

    cancel () {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'app-category-service-modal',
    template: ''
})

export class CategoryServiceModalComponent implements OnInit, OnDestroy {
    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private categoryModalService: CategoryModalService
    ) {}

    ngOnInit () {
        this.routeSub = this.route.params.subscribe(params => {
            if (params['id']) {
                this.modalRef = this.categoryModalService.open(CategoryServiceManagementModalComponent, params['id']);
            } else {
                this.modalRef = this.categoryModalService.open(CategoryServiceManagementModalComponent);
            }
        });
    }

    ngOnDestroy () {
        this.routeSub.unsubscribe();
    }
}
