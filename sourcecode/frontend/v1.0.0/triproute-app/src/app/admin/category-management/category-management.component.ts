import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { CategoryModalService } from './category-modal.service';
import { CategoryManagementModalComponent } from './category-management-modal.component'

@Component({
    selector: 'app-category-management',
    templateUrl: 'category-management.component.html'
})

export class CategoryManagementComponent implements OnInit, OnDestroy {
    stateParam: String = '';

    constructor (
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private categoryModalService: CategoryModalService
    ) {}

    ngOnInit () {
        this.activatedRoute.params.subscribe(params => {
            this.stateParam = params['state'];
        })
    }

    ngOnDestroy () {}

    createNewCategory () {
        this.categoryModalService.open(CategoryManagementModalComponent);
    }
}