import { Routes } from '@angular/router';

import { CategoryManagementComponent } from './category-management.component';
import { CategoryServiceManagementComponent } from './category-service-management.component';

export const categoryManagementRoutes = [
  {
    path: 'category-management',
    component: CategoryManagementComponent,
    data: {
        pageTitle: 'Quản lý danh mục bài viết'
    }
  },
  {
    path: 'category-service-management',
    component: CategoryServiceManagementComponent,
    data: {
        pageTitle: 'Quản lý danh mục dịch vụ'
    }
  }
];

export const categoryModalRoutes: Routes = [
  {
    path: 'category-management-add',
    component: CategoryManagementComponent,
    outlet: 'popup'
  },
  {
    path: 'category-management-edit/:id',
    component: CategoryManagementComponent,
    outlet: 'popup'
  },
  {
    path: 'category-service-management-add',
    component: CategoryServiceManagementComponent,
    outlet: 'popup'
  },
  {
    path: 'category-service-management-edit/:id',
    component: CategoryServiceManagementComponent,
    outlet: 'popup'
  }
];
