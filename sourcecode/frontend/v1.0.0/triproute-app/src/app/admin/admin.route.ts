import { Routes } from '@angular/router';

import {
    adminLoginRoute,
    dashboardRoute,
    userManagementRoutes,
    destinationManagementRoutes,
    destinationModalRoutes,
    countryManagementRoutes,
    countryModalRoutes,
    categoryManagementRoutes,
    categoryModalRoutes,
    postManagementRoutes,
    serviceManagementRoutes,
    routeManagementRoutes
} from './';

let ADMIN_ROUTES = [
    adminLoginRoute,
    dashboardRoute,
    ...userManagementRoutes,
    ...destinationManagementRoutes,
    ...countryManagementRoutes,
    ...categoryManagementRoutes,
    ...postManagementRoutes,
    ...serviceManagementRoutes,
    ...routeManagementRoutes
];


export const adminState: Routes = [
  {
    path: 'admin',
    data: {
        authorities: ['ROLE_ADMIN']
    },
    canActivate: [],
    children: ADMIN_ROUTES
  },
    ...destinationModalRoutes,
    ...countryModalRoutes,
    ...categoryModalRoutes
];
