import { Component, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, NavigationEnd, RoutesRecognized, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { AuthenticationService } from '../../shared/services/authentication.service';

@Component ({
  selector: 'app-root',
  templateUrl: 'main.component.html'
})

export class MainComponent implements OnInit {

  public bodyClass: string = "";

  isShowHeader: boolean = false;
  isShowSidebar: boolean = false;
  isShowFooter: boolean = false;

  constructor (
    private titleService: Title,
    private router: Router,
    private authService: AuthenticationService
  ) {}

  ngOnInit () {
    this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.titleService.setTitle(this.getPageTitle(this.router.routerState.snapshot.root));
        }
    });

    let body = document.getElementsByTagName('body')[0];

    if (!this.authService.isLoggedIn()) {
      this.isShowHeader = true;
      this.isShowSidebar = true;
      this.isShowFooter = true;
      body.classList.add('body-adm');
    } else {
      body.classList.add('body-login');
    }
  }

  private getPageTitle(routeSnapshot: ActivatedRouteSnapshot) {
    let title: string = (routeSnapshot.data && routeSnapshot.data['pageTitle']) ? routeSnapshot.data['pageTitle'] : 'Administartor';

    if (routeSnapshot.firstChild) {
      title = this.getPageTitle(routeSnapshot.firstChild) || title;
    }

    return title;
  }

  isAuthenticated () {
    return true;
  }

  public getBodyClass () {
    return 'body-adm';
  }
}
