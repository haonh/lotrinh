import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-triproute-error',
    templateUrl: 'error.component.html'
})
export class ErrorComponent implements OnInit {

    errorMessage: string;
    error403: boolean;

    constructor () {}

    ngOnInit () {}
}
