import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../../shared/services/authentication.service';


@Component ({
  selector: 'app-triproute-header',
  templateUrl: 'header.component.html'
})

export class HeaderComponent implements OnInit {
  constructor (
    private authService: AuthenticationService
  ) {}

  ngOnInit () {
  }
}
